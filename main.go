package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

type arrayFlags []string

type backendServer struct {
	DeadCount int    `json:"deadcount"`
	LiveCount int    `json:"livecount"`
	Address   string `json:"address"`
	Health    string `json:"health"`
	Weight    int    `json:"weight"`
}

type backendServers struct {
	sync.RWMutex
	servers  map[string]*backendServer
	maxDead  int
	minAlive int
	secret   string
	delay    time.Duration
	timeout  time.Duration
}

var (
	// Version of current binary
	Version string
	// BuildDate of current binary
	BuildDate string
)

func main() {
	var backs, add, del arrayFlags
	var address, secret, health, level, server string
	var min, max int
	var delay, timeout time.Duration
	var dump, version bool

	flag.StringVar(&address, "address", "127.0.0.1:8888", "listen address")
	flag.StringVar(&server, "server", "http://127.0.0.1:8888", "server address (client mode)")
	flag.Var(&add, "add", "backend server to add (client mode)")
	flag.Var(&del, "del", "backend server to delete (client mode)")
	flag.BoolVar(&dump, "dump", false, "dump servers (client mode)")
	flag.Var(&backs, "back", "backend server in the form 'url,health_uri,weight' (can be specified several times)")
	flag.StringVar(&health, "health", "", "health check uri for backend servers")
	flag.StringVar(&secret, "secret", "", "Secret token for administration")
	flag.IntVar(&max, "max", 3, "Maximum failed attempts before being considered dead")
	flag.IntVar(&min, "min", 2, "Minimum successful attempts before being considered alive")
	flag.DurationVar(&delay, "delay", time.Duration(1*time.Second), "health check delay interval")
	flag.DurationVar(&timeout, "timeout", time.Duration(2*time.Second), "health check timeout interval")
	flag.StringVar(&level, "level", "info", "log level")
	flag.BoolVar(&version, "version", false, "displays version")

	flag.Parse()

	if version {
		fmt.Printf("301 version %s (built %s)\n", Version, BuildDate)
		os.Exit(0)
	}

	lvl, err := logrus.ParseLevel(level)
	if err != nil {
		logrus.Fatalf("unable to use '%s' as loglevel; please choose among trace, debug, info and error", err)
	}
	logrus.SetLevel(lvl)

	// Client mode
	if len(add) != 0 {
		logrus.Debugf("adding backends %s", add)
		updateBackends("POST", add, server, secret)
	}

	if len(del) != 0 {
		logrus.Debugf("deleting backends %s", del)
		updateBackends("DELETE", del, server, secret)
	}

	if dump {
		logrus.Debugf("dumping backends")
		dumpBackends(server, secret)
	}

	// Exit if any client operation was issued
	if len(add)+len(del) > 0 || dump {
		os.Exit(0)
	}

	// Now on to server mode
	backends := &backendServers{
		secret:   secret,
		minAlive: min,
		maxDead:  max,
		servers:  make(map[string]*backendServer),
		delay:    delay,
		timeout:  timeout,
	}

	for _, b := range backs {
		back, err := backToBack(b)
		if err != nil {
			logrus.Fatalf("unable to add '%s' as backend: %v", b, err)
		}
		logrus.Infof("adding backend %s in our store", back.Address)
		backends.add(back)
	}

	logrus.Infof("starting backend check goroutine")
	go backends.checkBackends()

	logrus.Infof("listening on %s", address)
	s := &http.Server{
		Addr:           address,
		ReadTimeout:    2 * time.Second,
		WriteTimeout:   2 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	http.DefaultServeMux.HandleFunc("/", backends.redirect)
	http.DefaultServeMux.HandleFunc("/admin", backends.manage)

	logrus.Fatal(s.ListenAndServe())
}

func dumpBackends(server, secret string) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, "GET", server+"/admin", nil)
	if err != nil {
		logrus.Errorf("unable to build dump request: %v", err)
		return
	}
	req.Header.Set("Authorization", "Bearer "+secret)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		logrus.Warnf("unable dump servers: %v", err)
		return
	}

	if resp.StatusCode != http.StatusOK {
		logrus.Errorf("error dumping servers, got status %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.Errorf("unable to read response body: %v", err)
		return
	}
	var prettyJSON bytes.Buffer
	err = json.Indent(&prettyJSON, body, "", "  ")
	if err != nil {
		logrus.Errorf("unable to prettyfy response body: %v", err)
		return
	}
	fmt.Printf("%s\n", prettyJSON.String())
}

func updateBackends(method string, bcks arrayFlags, server, secret string) {
	for _, bck := range bcks {
		logrus.Debugf("%s backend '%s'", method, bck)
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		b, err := backToBack(bck)
		if err != nil {
			logrus.Errorf("unable to %s backend server (format) '%s': %v", method, bck, err)
			continue
		}
		jsbck, err := json.Marshal(b)
		if err != nil {
			logrus.Errorf("unable to %s backend server (encode) '%s': %v", method, bck, err)
			continue
		}

		req, err := http.NewRequestWithContext(ctx, method, server+"/admin", bytes.NewBuffer(jsbck))
		if err != nil {
			logrus.Errorf("unable to %s backend server (build request) '%s': %v", method, bck, err)
			continue
		}
		req.Header.Set("Authorization", "Bearer "+secret)
		req.Header.Set("Content-Type", "application/json")
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			logrus.Warnf("unable to %s backend server (POST) '%s': %v", method, bck, err)
			continue
		}

		if resp.StatusCode != http.StatusCreated && resp.StatusCode != http.StatusAccepted {
			logrus.Errorf("%s rejected for backend server '%s': %d", method, bck, resp.StatusCode)
			continue
		}
		logrus.Infof("backend '%s' added", bck)
	}
}

func backToBack(b string) (*backendServer, error) {
	parts := strings.Split(b, ",")
	if len(parts) != 3 {
		return nil, fmt.Errorf("unable to add '%s' as backend; please use the form 'http://some.server,/health,5'", b)
	}

	wgt, err := strconv.Atoi(parts[2])
	if err != nil {
		return nil, err
	}
	return &backendServer{
		Address: parts[0],
		Health:  parts[1],
		Weight:  wgt,
	}, nil
}

func (b *backendServers) redirect(w http.ResponseWriter, r *http.Request) {
	back, err := b.pick()
	if err != nil {
		logrus.Warnf("error picking backend: %v", err)
		message := fmt.Sprintf(`{"error": "%v"}`, err)
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte(message))
		return
	}

	logrus.Debugf("returning %s", back.Address)
	// fmt.Printfln(strings.TrimRight(back.Address, "/") + r.URL.Path)
	w.Header().Set("Location", strings.TrimRight(back.Address, "/")+r.URL.Path)
	w.WriteHeader(http.StatusFound)
}

func (b *backendServers) manage(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Authorization") != "Bearer "+b.secret {
		w.Header().Add("X-WTF", "you must be kidding right ?")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	switch r.Method {
	case http.MethodGet:
		d, err := b.dump()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(d)
		return

	case http.MethodPost:
		back := &backendServer{}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logrus.Errorf("unable to read body: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.Unmarshal(body, back)
		if err != nil {
			logrus.Errorf("unable to unmarshal body: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		logrus.Infof("got add request for '%s' from %s", back.Address, r.RemoteAddr)
		err = b.add(back)
		if err != nil {
			logrus.Errorf("unable to add backend server: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusCreated)
		return

	case http.MethodDelete:
		logrus.Infof("got DELETE request from %s", r.RemoteAddr)
		back := &backendServer{}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logrus.Errorf("unable to read body for DELETE: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err = json.Unmarshal(body, back)
		if err != nil {
			logrus.Errorf("unable to unmarshal body for DELETE: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		logrus.Infof("got DELETE request for '%s' from %s", back.Address, r.RemoteAddr)

		err = b.delete(back.Address)
		if err != nil {
			logrus.Errorf("unable to DELETE backend server: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusAccepted)
		return
	}
}

func (b *backendServers) checkBackends() {
	// This is too basic
	// on check can block all the others
	// we should spawn goroutines and reply in a channel

	// Get those once for all
	b.RLock()
	delay := b.delay
	timeout := b.timeout
	b.RUnlock()

	for {
		temp := map[string]backendServer{}

		// Get a copy and release lock since we might be here for a while
		b.RLock()
		for k, v := range b.servers {
			temp[k] = *v
		}
		b.RUnlock()

		for k, v := range temp {
			address := strings.TrimRight(k, "/") + "/" + strings.TrimLeft(v.Health, "/")
			logrus.Tracef("setting healthchk address to '%s' for backend %s", address, k)
			if httpCheck(address, timeout) {
				b.incrementLive(k)
			} else {
				b.incrementDead(k)
			}
		}
		time.Sleep(delay)
	}

}

func (b *backendServers) add(back *backendServer) error {
	b.Lock()
	defer b.Unlock()

	b.servers[back.Address] = back
	return nil
}

func (b *backendServers) pick() (*backendServer, error) {
	type weightEntry struct {
		back   string
		weight int
	}

	b.RLock()
	defer b.RUnlock()

	// Create a weight slice
	weights := []weightEntry{}

	totalWeight := 0
	for _, v := range b.servers {
		if v.DeadCount < b.maxDead && v.LiveCount >= b.minAlive {
			totalWeight += v.Weight
			weights = append(weights, weightEntry{back: v.Address, weight: v.Weight})
		}
	}

	// No weight, so no available backends
	if totalWeight == 0 {
		return nil, fmt.Errorf("no backends available")
	}

	// Pick a random number
	r := rand.Intn(totalWeight)

	counter := 0
	selected := ""
	// Find on with ordered entry we fall when waling the slice and counting the weight
	for _, bk := range weights {
		counter += bk.weight
		if counter > r {
			// We have a lucky winner
			selected = bk.back
			break
		}
	}

	// This should not happen
	if selected == "" {
		return nil, fmt.Errorf("we have %d live backends but unable to select one (bug ?)", len(weights))
	}

	return b.servers[selected], nil
}

func (b *backendServers) dump() ([]byte, error) {
	b.RLock()
	defer b.RUnlock()

	return json.Marshal(b.servers)
}

func (b *backendServers) delete(back string) error {
	b.Lock()
	defer b.Unlock()

	delete(b.servers, back)
	return nil
}

func (b *backendServers) incrementDead(back string) {
	b.Lock()
	defer b.Unlock()

	b.servers[back].DeadCount++
	b.servers[back].LiveCount = 0
}

func (b *backendServers) incrementLive(back string) {
	b.Lock()
	defer b.Unlock()

	if b.servers[back] != nil {
		b.servers[back].LiveCount++
		if b.servers[back].LiveCount > b.minAlive {
			b.servers[back].DeadCount = 0
		}
	} else {
		logrus.Warnf("backend removed during HTTP helth check")
	}
}

func httpCheck(url string, timeout time.Duration) bool {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	req, _ := http.NewRequestWithContext(ctx, "GET", url, nil)

	start := time.Now()
	resp, err := http.DefaultClient.Do(req)
	stop := time.Now()

	if err != nil {
		logrus.Warnf("unable to GET health check for %s: %v", url, err)
		return false // ctx cancel caught
	}

	if resp.StatusCode < 400 {
		logrus.Tracef("healthcheck success for %s in %d ms", url, stop.Sub(start)/time.Millisecond)

		return true
	}

	logrus.Warnf("bad response hitting health check for %s: %d", url, resp.StatusCode)
	return false
}

func (i *arrayFlags) String() string {
	return strings.Join(*i, ",")
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}
