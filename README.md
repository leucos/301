# 301

[![build status](https://gitlab.com/leucos/301/badges/master/pipeline.svg)](https://gitlab.com/leucos/301/commits/master)
[![coverage report](https://gitlab.com/leucos/301/badges/master/coverage.svg)](https://gitlab.com/leucos/301/-/commits/master)
[![License: WTFPL](https://img.shields.io/badge/License-WTFPL-brightgreen.svg)](http://www.wtfpl.net/about/)

This tool acts as a "loadbalancer" returning only 301 replies.

It can be usefull if you do not want to handle traffic to backends, but just
send redirects to them.

301 checks if the backends are alive before sending back `Location` headers
containing their address.

It has a dual mode: you run it either as a server or as client to add/remove
weighted backends. Backends can be added or removed using an API too.

## Quick start

### Start a server

```bash
301 -secret foo
```

By default, the server listens on `127.0.0.1:8888`; you can change this with
`-address`. The `-secret` protects the admin API.

### Check backends in the server

```bash
301 -dump -secret foo
```

This returns a JSON structure with the server list:

```json
{
  "https://some.server": {
    "deadcount": 0,
    "livecount": 1,
    "address": "https://some.server",
    "health": "/",
    "weight": 6
  },
  "https://other.server": {
    "deadcount": 0,
    "livecount": 1,
    "address": "https://other.server",
    "health": "/health",
    "weight": 5
  }
}
```

The curl-equivalent request is:

```bash
curl -H"Authorization: Bearer foo" 127.0.0.1:8888/admin
```

### Add a backend to the server

When adding a server, you must provide:

- an URL to redirect to
- a health check endpoint
- a weight for selection

Those values must be comma-separated. For instance:

```bash
301 -add http://myserver:1234,/health,5 -secret foo
```

The curl-equivalent request is:

```bash
curl -xPOST -H"Authorization: Bearer foo" 127.0.0.1:8888/admin --data '{ "address": "http://myserver:1234", "health": "/health", "weight": 5 }'
```

### Deleting a backend from the server

for deletion, only the nae is required:

```bash
301 -del http://myserver:1234 -secret foo
```

The curl-equivalent request is:

```bash
curl -xDELETE -H"Authorization: Bearer foo" 127.0.0.1:8888/admin --data '{ "address": "http://myserver:1234" }'
```

## Usage

Options:

- `-add`: add backend to list of servers (format: `url,health_uri,weight`)
- `-address`: listen address for server (default "127.0.0.1:8888")
- `-back <value>`: backend server to add when starting server, in the form
  `url,health_uri,weight` (can be specified several times)
- `-del <value>`: backend server to delete (client mode)
- `-delay <duration>`: health check delay interval (default `1s`); duration as
  golang value (e.g. s, m, h suffix)
- `-dump`: dump currently configured backends (client mode)
- `-health <string>`: health check uri for backend servers
- `-level <string>`: log level (default `info`; possible values `panic`,
  `fatal`, `error`, `warn`, `info`, `debug`))
- `-max <int>`: maximum failed attempts before a backend is considered dead
  (default `3`)
- `-min <int>`: minimum successful attempts before being considered alive
  (default `2`)
- `-secret <string>`: secret token for administration (via command or via
  `curl`)
- `-server <string>`: server address for client mode (default
  `http://127.0.0.1:8888`)
- `-timeout <duration>`: health check timeout interval (default `2s`)

## Docker

Images are avialable in the
[registry](https://gitlab.com/leucos/301/container_registry).

You just have to specify the topns to run the container, e.g.:

```bash
$ docker run -p 127.0.0.1:8888:8888 registry.gitlab.com/leucos/301:v0.0.2 -address 0.0.0.0:8888 -back https://example.com,/,1
time="2020-05-21T21:05:42Z" level=info msg="adding backend https://example.com in our store"
time="2020-05-21T21:05:42Z" level=info msg="starting backend check goroutine"
time="2020-05-21T21:05:42Z" level=info msg="listening on 0.0.0.0:8888"
```

## Examples

### start server with 2 backends

Start with backends `https://foo.com` & `https://bar.org`, having health checks
respectively available at `https://foo.com/health` and `https://bar.org/` and
weigths 5 and 6.
Set secret for management to `foo`

```bash
301 -back https://foo.com,/health,5 -back https://bar.org,/,6 -secret foo -level debug
```

### add a backend to a running server

Add another backend `https://baz.org` with health check at /test and weight 2:

```bash
301 -add https://baz.org,/test,2 -secret foo
```

## Building

```bash
git clone https://gitlab.com/leucos/301.git
cd 301
go build .
```

## TODO

Improve healthchecks by preventing one slow/unreachable server to block other
healthchecks.

## Licence

WTFPL