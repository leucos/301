FROM golang:1.14 as builder

ARG version
ARG builddate

WORKDIR /go/src/gitlab.com/leucos/301

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# hadolint ignore=SC2016
RUN GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0 \
    go build \
    -tags release \
    -ldflags "-w -extldflags \"-static\" -X main.Version=${version} -X main.BuildDate=${builddate}" -a \
    -o /go/bin/301

# hadolint ignore=DL3006
FROM gcr.io/distroless/base

COPY --from=builder /go/bin/301 /usr/local/bin/301

ENTRYPOINT ["/usr/local/bin/301"]